#!/usr/bin/python3
#####################################
## (C) Jesusalva, 2018-2024
## Published under the MIT License
## Usage: ./ledit.py in.py pt_BR
#####################################

import requests, json, traceback, subprocess, sys, time, os, uuid, base64
import zipfile, shutil, webbrowser
import polib
#from googletrans import Translator
from deep_translator import GoogleTranslator # ChatGptTranslator, DeeplTranslator
import tkinter as tk
from tkinter.messagebox import showinfo, showerror, askyesno
from functools import partial

## Prepare some basic stuff
execute=subprocess.call
sleep = time.sleep

class HoverButton(tk.Button):
    def enter(self, e):
        self["background"] = self["activebackground"]
    def exit(self, e):
        self["background"] = self.defaultBackground

    def __init__(self, **kw):
        tk.Button.__init__(self, **kw)
        self.defaultBackground = self["background"]
        self.bind("<Enter>", self.enter)
        self.bind("<Leave>", self.exit)

## IF Then Else (IFTE)
def ifte(ifs, then, elses):
    if (ifs):
        return then
    else:
        return elses

# Search for array[?][key]==search in an array of dicts
# Returns the index, or returns -1
def dl_search_idx(array, key, search):
    try:
        r=next((i for i, item in enumerate(array) if item[key] == search), None)
    except:
        traceback.print_exc()
        r=-1
    return ifte(r is None, -1, r)

## Open a game website
def open_url(link, *etc):
    webbrowser.open_new(link)
    return

## Only meaningful on Linux, or base things
#ttl=Translator()
os.environ["APPIMAGELAUNCHER_DISABLE"]="1"

## Retrieve file name
if len(sys.argv) >= 2:
    fn = sys.argv[1]
else:
    fn = "in.po"

## Retrieve language
if len(sys.argv) >= 3:
    ln = sys.argv[2]
else:
    ln = "pt_BR"

## With deep_tl we need to pass this later
ttl=GoogleTranslator(source="en", target=ln.split("_")[0])

## Open the PO file and report current completion
print("Opening \"%s\"...", fn)
po=polib.pofile(fn)
cnt=0
print("Current progress: "+str(po.percent_translated())+"%")

## Define the scopes, they should be completed in order
scope_list=[("Nard Ship", "npc/002-", "npc/000-"),
            ("Candor",    "npc/005-", "npc/006-2"),
            ("Databases", "db/", "npc/functions", "npc/items", "npc/config", "npc/craft", "npc/016-"),
            ("Tulimshar", "npc/003-", "npc/007-"),
            ("Hurnscald", "npc/012-", "npc/013-", "npc/014-", "npc/015-"),
            ("Halinarzo", "npc/009-", "npc/011-"),
            ("Canyon",    "npc/004-", "npc/010-"),
            ("Land Fire", "npc/017-", "npc/018-"),
            ("M Academy", "npc/027-", "npc/magic"),
            ("Kaizei",    "npc/019-", "npc/020-", "npc/021-", "npc/022-"),
            ("Frostia",   "npc/023-", "npc/024-", "npc/001-7"),
            ("Events",    "npc/001-", "npc/006-", "npc/030-"),
            ("Fortress",  "npc/025-", "npc/026-", "npc/029-"),
            ("Special",   "npc/031-", "npc/032-", "npc/033-", "npc/034-", "npc/soren"),
            ("Kamelot",   "npc/042-", "npc/guilds"),
            ("System",    "npc/boss", "npc/botcheck", "npc/commands", "npc/dev", "npc/sec_pri", "npc/test", "conf/")]
            #("Other...")]


#################################################################################
polist=[]; current=""
def _setHome(_hom):
    global polist, po, current
    _home = _hom.get()
    print("Loading %s" % _home)
    polist=[]
    entry =[]
    for s in scope_list:
        if s[0] == _home:
            entry=s
            break
    if entry == []:
        return
    #entry.remove(0) ## <- You can't remove something from a tuple

    for e in po.untranslated_entries():
        # e.msgid  -> English text
        # e.msgstr -> Translated text
        # e.comment -> code information
        # e.occurrences -> array of tuples where it shows at
        for o in e.occurrences:
            if e in polist:
                break
            for fp in entry:
                if o[0].startswith(fp):
                    polist.append(e)
                    break
    print("Domain changed!")
    current=_home
    _build_screen(destroy=True)
    return

## Save the translation to the po file
def save(widget):
    global polist, po, cnt
    translated=widget.get("1.0", "end")
    translated=translated.replace('\r','')
    if translated.endswith("\n"):
        translated=translated[:-1]
    print("»" + translated)
    polist[0].msgstr=translated
    polist.pop(0)
    po.save()
    cnt+=1
    _build_screen(destroy=True)
    return

#################################################################################
## Build Tkinter interface
root=tk.Tk()
root.title("Moubootaur Legends l'edit")

## Initialize the icon
try:
    _favicon = tk.PhotoImage(file = "favicon.png")
except:
    from _img import __favicon
    _favicon = tk.PhotoImage(data = __favicon)

root.iconphoto(True, _favicon)
canva=None

## Core function for the screen
def _build_screen(destroy=False):
    global canva, ttl, ln
    if destroy:
        canva.destroy()
    canva = tk.Canvas(root, width=600, height=600, bg="#0c3251")
    canva.pack()

    ## Prompt to select a domain
    if not len(polist):
        label2 = tk.Label(root, text='Work Domain:', bg="#0c3251", fg="#fff")
        label2.config(font=('helvetica', 12))
        canva.create_window(50, 50, window=label2)

    ## Domain selection core
    ## It's partly ommited when translating
    _home = tk.StringVar()
    if current != "":
        _home.set(current)
    else:
        _home.set("-- Set a domain --")
    lista = []
    for s in scope_list:
        lista.append(s[0])
    drop = tk.OptionMenu(canva, _home, *lista)
    drop.config(bg="#cc6600", fg="#fff") # 0c3251
    drop["menu"].config(bg="#cc6600", fg="#fff") # 0c3251
    if not len(polist):
        canva.create_window(220, 150, window=drop)
    else:
        canva.create_window(220, 550, window=drop)

    hosav = HoverButton(text="→", command=partial(_setHome, _home), bg="#cc6600", fg="#fff", activebackground="#ee9933")
    if not len(polist):
        canva.create_window(375, 150, window=hosav)
    else:
        canva.create_window(375, 550, window=hosav)

    ## Iterate over polist and pick the first entry if possible
    if len(polist):
        e=polist[0]
        ## Add the original text
        label1 = tk.Text(root, state="normal", width=50, height=3, background="#0c2241", foreground="#fff", wrap="word")
        #label1.config(font=('helvetica', 14))
        label1.insert("end", e.msgid)
        label1["state"]="disabled"
        canva.create_window(220, 40, window=label1)

        ## Add the translation text
        entry1 = tk.Text(root, state="normal", width=50, height=3, background="#0c2241", foreground="#fff", wrap="word")
        try:
            #entry1.insert("end", ttl.translate(str(e.msgid), src='en', dest=ln).text)
            entry1.insert("end", ttl.translate(str(e.msgid)))
        except:
            traceback.print_exc()
            pass
        #entry1.config(font=('helvetica', 14), bg="#ccc")
        canva.create_window(220, 120, window=entry1)

        ## Add the text comment (original text)
        label3=tk.Text(root, state="normal", width=62, height=4, background="#0c3251", foreground="#fff", wrap="word")
        try:
            label3.insert("end", e.comment)
        except:
            label3.insert("end", "--")
        label3.config(font=('monospace', 10))
        label3["state"]="disabled"
        canva.create_window(300, 300, window=label3)

        ## Report where the original text is
        label4 = tk.Label(root, text=e.occurrences, bg="#0c3251", fg="#fff", width=62)
        label4.config(font=('helvetica', 14))
        canva.create_window(300, 362, window=label4)

        button1 = HoverButton(text='Save', command=partial(save, entry1), bg="#cc6600", fg="#fff", activebackground="#ee9933")
        canva.create_window(300, 500, window=button1)
    return

# Perpetual loop
_build_screen()
root.mainloop()
print("\033[33;1mThanks for translating!\033[0m")
print("Translated %d entries" % cnt)
print("Final progress: "+str(po.percent_translated())+"%")

