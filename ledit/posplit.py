#!/usr/bin/python3
#####################################
## (C) Jesusalva, 2024
## Published under the MIT License
#####################################

import traceback, sys, time, os
import polib

## Create directory if it doesn't exist out
os.makedirs("out", exist_ok=True)

## The file which we're splitting; to merge use msgcat
if len(sys.argv) >= 2:
    fn = sys.argv[1]
else:
    fn="in.po"

## Open the PO file and report current completion
print("Opening \"%s\"..." % fn)
po=polib.pofile(fn)
cnt=0
print("Current string count: %d" % len(po))

domains=("Nard", "Candor", "Databases", "Tulimshar", "Hurnscald", "Halinarzo", "Canyon", "LoF", "Academy", "Kaizei", "Frostia", "Events", "Fortress", "Special", "Kamelot", "System", "Other", "Error")

deeds={}
for d in domains:
    deeds[d] = polib.POFile()
    deeds[d].metadata = {
    'Project-Id-Version': '1.0',
    'Report-Msgid-Bugs-To': 'admin@tmw2.org',
    'POT-Creation-Date': '1970-01-01 00:00+0000',
    'PO-Revision-Date': '1970-01-01 00:00+0000',
    'Last-Translator': 'Auto Generated <admin@tmw2.org>',
    'Language-Team': 'English <admin@tmw2.org>',
    'MIME-Version': '1.0',
    'Content-Type': 'text/plain; charset=utf-8',
    'Content-Transfer-Encoding': '8bit',
}

#################################################################################
polist=[]; current=""

# Use msgcat to merge the po files
# msgcat -o out.po *.po
for entry in po:
    found=False
    for k in domains:
        if "domain/%s" % (k) in repr(entry.occurrences):
            deeds[k].append(entry)
            found=True
            break
    if not found:
        deeds["Error"].append(entry)

wc=0; awc=0
for k in domains:
    awc+=wc
    wc=0
    for w in deeds[k]:
        wc+=len(w.msgid.split())
    print("\033[1m%s\033[0m: %s%% completed, %d strings, %d words" % (k, str(deeds[k].percent_translated()), len(deeds[k]), wc))
    if wc > 0:
        deeds[k].save('out/domain_%s.po' % k)
    else:
        print("\033[31;1m%s was not saved.\033[0m" % k)

awc += wc; wc = 0
print("Finished with %d words total." % awc)
