// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  This is the “015-8 mapfile” black box.
//  It contains the answers to all the riddles.
//  This file is kept outside the main tree for translation purposes.
//  You thought you could cheat the answers, eh?

//  Syntax:
//  0158_Riddle_BlackBox(  )
//  Returns true on success, false on failure.
function	script	0158_Riddle_BlackBox	{
    .@r$="secret";
    .@l$=l("secret");

    mesc l("When I was alive, I brought people to their destiny.");
    mesc l("One day, I was provoked. Others came to my aid,");
    mesc l("But all I could do was a bite.");
    mesc l("To my murders, I gave them a full meal.");
    mesc l("And in a hut, a lone woman mourned my death.");
    mes "";
    input .@ans$;

    return riddlevalidation(.@ans$, .@r$[.@e], .@l$[.@e]);
}

