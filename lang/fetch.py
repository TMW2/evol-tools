#! /usr/bin/env python3
# -*- coding: utf8 -*-
#
# Copyright (C) 2018  TMW-2
# Author: Jesusalva, ntsneto

from transifex.api import transifex_api
import requests

## Master translation control, see data/langs/langs.txt as master reference
TTL={"English": "en", "Portuguese (Brazil)": "pt_BR", "French": "fr", "German": "de", "Spanish": "es", "Russian": "ru"}

# Load API Token from apy_token_transifex.txt. You can generate one at https://www.transifex.com/user/settings/api/.

with open("token.txt", "r") as f:
    token = f.read()
    token = str(token).replace('\n','')

transifex_api.setup(auth=token)

organization_name = "arctic-games"
project_name = "moubootaur-legends"
resource_name = "serverdata"

organization = transifex_api.Organization.get(slug=organization_name)

print(f"Getting project details from organization {organization_name}")
project = organization.fetch("projects").get(slug=project_name)

print(f"Getting resource {resource_name} from project {project_name}")
resource = project.fetch("resources").get(slug=resource_name)

# Fetch all translations and record them at in/

print(f"Getting languages from project {project_name}")
languages = project.fetch("languages")
for lang in languages:
    url = transifex_api.ResourceTranslationsAsyncDownload.download(resource=resource, language=lang)
    translated_content = requests.get(url).text
    print(f"Fetching {lang.name}")
    try:
        code = TTL[lang.name]
    except:
        print(f"{lang.name} is unsupported, SKIPPED")
        continue
    with open(f"in/{code}.po", "w", encoding="UTF-8") as f:
        print(translated_content, file=f)

