#!/usr/bin/python3
## This makes sure all maps have a background (required by 4144 on empty map view)

import os
PATH='../../client-data/maps'
for mp in os.listdir(PATH):
    map = '%s/%s' % (PATH, mp)
    if not map.endswith('.tmx'):
        continue
    HASBG=False
    bf=[]
    with open(map) as f:
        for l in f:
            if "background" in l:
                HASBG=True
                break
            bf.append(l)
    if HASBG:
        continue
    with open(map, 'w') as f:
        for l in bf:
            n=f.write(l)
            if '<properties>' in l and not HASBG:
                n=f.write('  <property name="background0image" value="graphics/images/spit23loginwallpaper_800x600.png"/>\n')
                HASBG=True
