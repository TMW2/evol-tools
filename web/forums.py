#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup # pip3 install beautifulsoup4

jar = requests.cookies.RequestsCookieJar()
fakeUserAgent = "Mozilla/5.0 (X11; CrOS aarch64 13261.0.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4161.0 Safari/537.36"

with open("login.txt") as f:
    username = f.read().replace('\n', '')
with open("password.txt") as f:
    password = f.read().replace('\n', '')
sid = ""
forumId = 79
topicId = 21488

def isAuthed():
    global jar, sid
    return len(jar) >= 3 and len(sid) >= 1

## Obtain the SID
r=requests.get("https://forums.themanaworld.org/index.php", headers={"User-Agent": fakeUserAgent})

## Create the initial (real) cookie jar
jar = r.cookies
for k in jar.keys():
    if k.endswith("_sid"):
        sid = r.cookies[k]

## Integrity check: Session ID
assert len(sid) > 1

## Pre-Login
r=requests.get("https://forums.themanaworld.org/ucp.php?mode=login&redirect=index.php&sid=%s" % sid, headers={"Origin": "https://forums.themanaworld.org", "Referer": "https://forums.themanaworld.org/", "User-Agent": fakeUserAgent}, cookies=jar)

## Retrieve creation time and token
soup = BeautifulSoup(r.text)
creationTag = None
tokenTag = None
for k in soup.find_all('input', type="hidden"):
    if k["name"] == "creation_time":
        creationTag = k["value"]
    elif k["name"] == "form_token":
        tokenTag = k["value"]

## Integrity check: Login Tags
assert creationTag is not None
assert tokenTag is not None


login = dict()
login["username"] = username
login["password"] = password
login["autologin"] = True #"on"
login["viewonline"] = True #"on"
login["redirect"] = "./ucp.php?mode=login&redirect=index.php"
login["creation_time"] = creationTag
login["form_token"] = tokenTag
login["sid"] = sid
#login["redirect"]="index.php"
login["login"]="Login"

## Login
r=requests.post("https://forums.themanaworld.org/ucp.php?mode=login&redirect=index.php&login=Login", headers={"Origin": "https://forums.themanaworld.org", "Referer": "https://forums.themanaworld.org/ucp.php?mode=login&redirect=index.php&sid=%s" % sid, "User-Agent": fakeUserAgent, "Content-Type": "application/x-www-form-urlencoded"}, cookies=jar, data=login, allow_redirects=False)

while r.status_code == 302:
    jar.update(r.cookies)
    print("Redirecting to %s..." % r.headers['Location'])
    r=requests.get(r.headers['Location'], cookies=jar)

del password
assert isAuthed()

#################################################################################
## Parse the news

def markup(r):
    r=r.replace('##0', '')
    r=r.replace('##1', '')
    r=r.replace('##2', '')
    r=r.replace('##3', '')
    r=r.replace('##4', '')
    r=r.replace('##5', '')
    r=r.replace('##6', '')
    r=r.replace('##7', '')
    r=r.replace('##8', '')
    r=r.replace('##9', '')
    r=r.replace('##B', '[b]')
    r=r.replace('##b', '[/b]')
    r=r.replace('[@@', '[url="')
    r=r.replace('|', '"]')
    r=r.replace('@@]', '[/url]')
    return r

def nn(r):
    return r.replace('\n', '')

SUBJECT=""
MESSAGE=""
with open("../update/news.txt") as f:
#with open("/home/jesusalva/projetos/tmw2/tools/update/news.txt") as f:
    for i in f:
        if ('##0 Actual Release: ##1' in i):
            SUBJECT=nn(i.replace('##0 Actual Release: ##1',''))
            MESSAGE+="[size=150][b]»%s[/b][/size]\n[hr]" % SUBJECT
        elif 'possible to skip music download under' in i:
            continue
        else:
            MESSAGE+=markup(i)

#################################################################################
## Reply-To something
rp=requests.get("https://forums.themanaworld.org/posting.php?mode=reply&f=%d&t=%d" % (forumId, topicId), headers={"Origin": "https://forums.themanaworld.org", "Referer": "https://forums.themanaworld.org/", "User-Agent": fakeUserAgent}, cookies=jar)

data = dict()
data["subject"] = SUBJECT
data["message"] = MESSAGE
data["post"] = "Submit"
data["topic_cur_post_id"] = None
data["creation_time"] = None
data["form_token"] = None
#data["lock_topic"] = False
#data["lock_post"] = False

soup = BeautifulSoup(rp.text)
for k in soup.find_all('input', type="hidden"):
    if k["name"] == "post":
        data["post"] = k["value"]
    elif k["name"] == "creation_time":
        data["creation_time"] = k["value"]
    elif k["name"] == "form_token":
        data["form_token"] = k["value"]
    elif k["name"] == "topic_cur_post_id":
        data["topic_cur_post_id"] = k["value"]

## Submit new form data
r=requests.post("https://forums.themanaworld.org/posting.php?mode=reply&f=%d&sid=%s&t=%d" % (forumId, sid, topicId), headers={"Origin": "https://forums.themanaworld.org", "Referer": "https://forums.themanaworld.org/posting.php?mode=reply&f=%d&t=%d" % (forumId, topicId), "User-Agent": fakeUserAgent}, cookies=jar, data=data, allow_redirects=False)


#################################################################################
## Logout
#for k in jar.keys():
#    if k.endswith("_sid"):
#        sid = jar.cookies[k]
r=requests.get("https://forums.themanaworld.org/ucp.php?mode=logout&sid=%s" % (sid), headers={"Origin": "https://forums.themanaworld.org", "Referer": "https://forums.themanaworld.org/", "User-Agent": fakeUserAgent}, cookies=jar)


