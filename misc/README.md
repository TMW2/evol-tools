# Maze Generation Script

Run `./maze.py <number of mazes> <width> <height>`, dimensions is in 3x3 tiles.
Currently, all mazes are square.

- Small: 30
- Medium: 45
- Great: 60
- Xtra: 75
- Absolute: 90

2. Remove the spurious comma from generated maps
3. Automap it in Tiled
4. Reorder the layers
5. Run `rewrite.py` in the art repository to add the Dream Tower version.

Keep the ORIGINAL version in art repository (the one before adding Dream Tower).
