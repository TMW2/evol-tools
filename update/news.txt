##0 Actual Release: ##1 19.4 Point of Interest
##0 Welcome to ##BTMW-2: Moubootaur Legends##b!

##0 By playing you agree and abide to the ##1##BTerms of Service##b##0, available at:
##1 [@@https://tmw2.org/legal|https://tmw2.org/legal@@]
##2 Last ToS update: 2021-09-15

##0 The spotlight of this release is the ##BPoint of Interest##b system. Do you see
##0 these random barrels, pans, wardrobes and boxes in some maps? Well, most won't,
##0 but a few of them might contain some goodies if you explore them enough.
##0 Including Zegas' barrels, which can now be partly repeated. Hopefully it's fun!
##0 If you're too broke, as long that you have a bank account, you can take the
##0 ##BMerchant Guild Requests##b for some quick cash.

##0 Several bugfixes were also made, and Piou Isles Chef Quest was included. You can
##0 also transmute in loop, and inflicting ##Bstatus ailments##b was improved,
##0 with arrows getting additional chance and more monsters being vulnerable to them.

##0 ##BPickaxe##b now boosts your defense against melee damage, and some balance
##0 adjustments were made. Enjoy the new ##Bskill icons##b which went live earlier.

##0 Ready to become a Moubootaur Legend now?
##0 Mana Source Team

##7 We want to express our gratitude here to ##BWoody##b, ##BSharli##b and ##BPovo##b for sponsoring this server.
##7 We would also want to kindly thank ##Bjesusalva##b, ##Bjak1##b and the GermanTMW Team
##7 for their sponsorship and help, and also to Software in Public Interest, Inc.
##7 Your donations in the US made through SPI are tax deductible.
##7 And by last, our gratitude to every developer and contributor who made this
##7 possible!

----

##0 [@@https://transifex.com/arctic-games/moubootaur-legends|Translators@@] are always welcome.
##1 Our Staff will never ask for your password. You are the sole responsible for
##1 its safety! You can contact a GM with ##B@request##b or calling one at #world.
##0 (If you want, it's possible to skip music download under Settings > Audio)

##0 Enjoy gaming, and leave feedback!

##9 -- Your TMW:ML Team
##9 January 2025

##0 You can check out this page for older entries:
##9 [@@https://tmw2.org/news|https://tmw2.org/news@@]
