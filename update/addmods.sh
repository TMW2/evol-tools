#!/usr/bin/env bash

# Copyright (C) 2010-2012  TMW2 Online
# Author: Andrei Karas (4144)

dir=`pwd`
mds='../../client-data/mods'

previous=`cat commit.txt`
rm files/xml_mods.txt
rm files/mod-*

cd ../../client-data/mods

FILES=`ls`

for file in $FILES; do
    echo "Uploading $file mod...."
    cd $file
    find . -type f | xargs zip -9 -r ../../../tools/update/files/mod-$file.zip
    cd $dir/files
    sum=`adler32 mod-$file.zip | awk '{print $2}'`
    echo "    <mod type=\"data\" group=\"$file\" file=\"mod-$file.zip\" hash=\"${sum}\" />" >> xml_mods.txt
    cp mod-$file.zip ../upload/
    cd ../$mds
done

cd $dir/files
cp xml_header.txt resources.xml
cat xml_footer.txt >>resources.xml
cat xml_mods.txt >>resources.xml
echo '</updates>' >>resources.xml
cp resources2.txt ../upload/
cp resources.xml ../upload/

