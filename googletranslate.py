#!/usr/bin/python

import polib
from googletrans import Translator
from time import sleep

ttl=Translator()
po=polib.pofile('in.po')
cnt=0
print "Current progress: "+str(po.percent_translated())+"%"
print "Initiating"

for e in po.untranslated_entries():
    if not "#" in e.msgid and not "%%" in e.msgid:
        cnt+=1
        try:
            e.msgstr=ttl.translate(str(e.msgid), src='en', dest='pt_BR').text
        except:
            pass
        sleep(0.25)
        print "%s\n-> %s" % (e.msgid, e.msgstr)
        if (cnt % 20) == 0:
            po.save()
            sleep(1) # Safety timer
            print "\033[33;1mSaved Successfully\033[0m"

po.save()
print "Translated %d entries" % cnt
print "Progress afterwards:"+str(po.percent_translated())+"%"

